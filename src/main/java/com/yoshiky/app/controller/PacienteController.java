package com.yoshiky.app.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.app.bean.RespuestaBean;
import com.yoshiky.app.model.Paciente;
import com.yoshiky.app.service.PacienteService;
import com.yoshiky.app.util.Constante;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

	private static final Logger log = LoggerFactory.getLogger(PacienteController.class);

	@Autowired
	private PacienteService pacienteService;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<RespuestaBean> registrar(@RequestBody Paciente paciente) {
		log.info("Inicia registro del paciente.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			Paciente pacienteBuscado = pacienteService.encontrarXId(paciente != null ? paciente.getId() : null);
			if (pacienteBuscado != null) {
				respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
				respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
				return new ResponseEntity<>(respuesta, HttpStatus.BAD_REQUEST);

			}

			pacienteService.crear(paciente);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);

		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@PostMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<RespuestaBean> actualizar(@RequestBody Paciente paciente) {
		log.info("Inicia actualización del paciente.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			pacienteService.crear(paciente);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<RespuestaBean> eliminar(@PathVariable("id") Integer id) {
		log.info("Inicia eliminación del paciente.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			pacienteService.eliminar(id);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Paciente>> listar() {
		log.info("Inicia metodo listar los pacientes.");
		List<Paciente> lstPacientes = new ArrayList<Paciente>();
		try {
			lstPacientes = pacienteService.encontrarTodos();
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lstPacientes, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<Paciente> listar(@PathVariable("id") Integer id) {
		log.info("Inicia metodo listar el paciente.");
		Paciente paciente = new Paciente();
		try {
			paciente = pacienteService.encontrarXId(id);
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(paciente, HttpStatus.OK);
	}

}
