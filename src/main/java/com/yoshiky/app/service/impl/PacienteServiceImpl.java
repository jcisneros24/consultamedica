package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.PacienteDao;
import com.yoshiky.app.model.Paciente;
import com.yoshiky.app.service.PacienteService;

@Service
public class PacienteServiceImpl implements PacienteService {

	private static final Logger log = LoggerFactory.getLogger(PacienteServiceImpl.class);

	@Autowired
	private PacienteDao pacienteDao;

	@Override
	public Paciente crear(Paciente t) {
		log.info("Inicia metodo crear Paciente");
		return pacienteDao.save(t);
	}

	@Override
	public Paciente actualizar(Paciente t) {
		log.info("Inicia metodo actualizar Paciente");
		return pacienteDao.save(t);
	}

	@Override
	public void eliminar(int id) {
		log.info("Inicia metodo eliminar Paciente");
		pacienteDao.delete(id);
	}

	@Override
	public Paciente encontrarXId(int id) {
		log.info("Inicia metodo encontrar por ID Paciente");
		return pacienteDao.findOne(id);
	}

	@Override
	public List<Paciente> encontrarTodos() {
		log.info("Inicia metodo encontrar todos Paciente");
		return pacienteDao.findAll();
	}

}
