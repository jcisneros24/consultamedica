package com.yoshiky.app.service;

import com.yoshiky.app.model.Paciente;

public interface PacienteService extends GenericsService<Paciente> {

}
